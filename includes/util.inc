<?php

/**
 * @file
 * Utility functions for use in the rawlog_reports module. This allows the other modules/incs/etc to focus on their 
 * functions related to their primary concerns.
 */

// Global holding the types found in the last parse
$rawlog_reports_types = array();

/**
 * Returns the latest log file in the rotation based on the variable for rawlog_logfile.
 */
function _rawlog_reports_latest_file() {
  return variable_get('rawlog_logfile', $_SERVER['DOCUMENT_ROOT'] .'/'. drupal_get_path('module', 'rawlog') .'/rawlog.log');
}

function _rawlog_reports_get_records($filename = NULL, $start = 0, $limit = 50) {
  $filename = $filename == NULL ? _rawlog_reports_latest_file() : $filename;
  $log_data = array(); //define the main array.
  
  
  $lines = new ReverseFileReader($filename);
  $filters = rawlog_reports_build_filter_query();
  if (!empty($filters['severities'])) {
    $severities = $filters['severities'];
  }
  if (!empty($filters['types'])) {
    $types = $filters['types'];
  }
  $i = 0;
  foreach($lines as $line) {
    preg_match(RAWLOG_REPORTS_REGEX, $line, $data);
    // if empty data leave
    if (empty($data[1])) {
      continue;
    }
    array_shift($data);
    if ((empty($severities) || in_array($data[2][1], $severities)) &&
        (empty($types) || in_array($data[3], $types)) &&
        ($i++ <= $limit)) {
      $log_data[] = new Record($data);
    }
    _rawlog_reports_add_type($data[3]);
  }
  return $log_data;
  exit();
  
  
  if (($handle = fopen($filename, "r")) !== FALSE) {
    $filters = rawlog_reports_build_filter_query();
    if (!empty($filters['severities'])) {
      $severities = $filters['severities'];
    }
  	$i = 0;
	while ($i < $limit && ($line = fgets($handle)) !== FALSE) {
      preg_match(RAWLOG_REPORTS_REGEX, $line, $data);
      array_shift($data);
      if (empty($severities) || in_array($data[2][1], $severities)) {
        $log_data[] = new Record($data);
      }
      _rawlog_reports_add_type($data[3]);
      $i++;
    }
    fclose($handle);
  }
  return $log_data;
}

function _rawlog_reports_icon_array() {
  return array(
    WATCHDOG_DEBUG    => '',
    WATCHDOG_INFO     => '',
    WATCHDOG_NOTICE   => '',
    WATCHDOG_WARNING  => theme('image', 'misc/watchdog-warning.png', t('warning'), t('warning')),
    WATCHDOG_ERROR    => theme('image', 'misc/watchdog-error.png', t('error'), t('error')),
    WATCHDOG_CRITICAL => theme('image', 'misc/watchdog-error.png', t('critical'), t('critical')),
    WATCHDOG_ALERT    => theme('image', 'misc/watchdog-error.png', t('alert'), t('alert')),
    WATCHDOG_EMERG    => theme('image', 'misc/watchdog-error.png', t('emergency'), t('emergency')),
  );
}

function _rawlog_reports_class_array() {
  return array(
    WATCHDOG_DEBUG    => 'dblog-debug',
    WATCHDOG_INFO     => 'dblog-info',
    WATCHDOG_NOTICE   => 'dblog-notice',
    WATCHDOG_WARNING  => 'dblog-warning',
    WATCHDOG_ERROR    => 'dblog-error',
    WATCHDOG_CRITICAL => 'dblog-critical',
    WATCHDOG_ALERT    => 'dblog-alert',
    WATCHDOG_EMERG    => 'dblog-emerg',
  );
}

function _rawlog_reports_add_type($type) {
  global $rawlog_reports_types;
  if (!is_array($rawlog_reports_types)) {
    $rawlog_reports_types = array();
  }
  if(!in_array($type, $rawlog_reports_types)) {
    $rawlog_reports_types[] = $type;
  }
}

/**
 * This function is very stateful at present. It will only report correctly if the _rawlog_reports-get_records method
 * has already been called. This is because it depends on the parsing of the log file to determine what types are 
 * contained within. 
 */
function _rawlog_reports_message_types() {
  global $rawlog_reports_types;
  return $rawlog_reports_types;
}

function _rawlog_reports_test_logging() {
  watchdog('rawlog_reports', t('testing log file DEBUG.'), NULL, WATCHDOG_DEBUG);
  watchdog('rawlog_reports', t('testing log file NOTICE.'), NULL, WATCHDOG_NOTICE);
  watchdog('rawlog_reports', t('testing log file WARN.'), NULL, WATCHDOG_WARNING);
  watchdog('rawlog_reports', t('testing log file ERROR.'), NULL, WATCHDOG_ERROR);
  watchdog('rawlog_reports', t('testing log file CRITICAL.'), NULL, WATCHDOG_CRITICAL);
}

class Record {

  public $severity;
  public $type;
  public $date;
  public $message;
  public $user;
  public $link;

  function __construct($data) {
    $this->severity = $data[2][1];
    $this->type = $data[3];
    $this->date = $data[0];
    $this->message = l(truncate_utf8($data[9], 56, TRUE, TRUE), 'admin/reports/rawlog-event/', array('html' => TRUE));
    $this->user = theme('username', $data[7]);
    $this->link = l('view', $data[5]);
  }
  
}

class ReverseFileReader extends SplFileObject {

  private $lineCount;
  private $loc;

  function __construct($filename, $use_include_path = true) {
    parent::__construct($filename, 'r', $use_include_path);
    $this->lineCount = 0;
    while(!$this->eof()) {
      $this->seek($this->lineCount);
      ++$this->lineCount;
    }
    $this->rewind();
  }

  function next() {
    $this->loc--;
    if(!$this->valid()) {
      return;
    }
    $this->seek($this->loc);
  }

  function key() {
    return $this->loc;
  }

  function valid() {
    return !($this->loc < 0 || $this->loc > $this->lineCount);
  }

  function rewind() {
    $this->seek($this->lineCount);
    $this->loc = $this->lineCount;
  }

}
