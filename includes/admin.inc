<?php

/**
 * @file
 * Report pages' callbacks for the rawlog_reports module.
 */


module_load_include('inc', 'rawlog_reports', "includes/util"); 

/**
 * Menu callback; displays a listing of log messages.
 */
function rawlog_reports_overview($type = NULL) {
  // _rawlog_reports_test_logging(); // TODO remove before release
  $icons = _rawlog_reports_icon_array();
  $classes = _rawlog_reports_class_array();

  $header = array(
    ' ',
    array('data' => t('Type'), 'field' => 'type'),
    array('data' => t('Date'), 'field' => 'wid', 'sort' => 'asc'),
    t('Message'),
    array('data' => t('User'), 'field' => 'name'),
    array('data' => t('Operations')),
  );

  foreach (_rawlog_reports_get_records() as $entry) {
    $rows[] = array('data' =>
      array(
        $icons[$entry->severity],
        t($entry->type),
        $entry->date,
        $entry->message,
        $entry->user,
        $entry->link,
      ),
      // Attributes for tr
      'class' => "dblog-". preg_replace('/[^a-z]/i', '-', $entry->type) .' '. $classes[$entry->severity]
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No log messages available.'), 'colspan' => 6));
  }

  $output = drupal_get_form('rawlog_reports_filter_form');
  $output .= theme('table', $header, $rows, array('id' => 'admin-rawlog'));
  $output .= theme('pager', NULL, 50, 0);

  return $output;
}

/**
 * Menu callback; generic function to display a page of the most frequent log events of a specified type.
 */
function rawlog_reports_top($type) {
  // TODO
}

/**
 * Menu callback; displays details about a log message.
 */
function rawlog_reports_event($id) {
 // TODO
}

/**
 * Build query for dblog administration filters based on session.
 */
function rawlog_reports_build_filter_query() {
  if (empty($_SESSION['rawlog_reports_overview_filter'])) {
    return;
  }
  $types = $_SESSION['rawlog_reports_overview_filter']['type'];
  $severities = $_SESSION['rawlog_reports_overview_filter']['severity'];
  return array(
    'types' => $types,
    'severities' => $severities,
  );
}


/**
 * List rawlog administration filters that can be applied.
 */
function rawlog_reports_filters() {
  $filters = array();

  foreach (_rawlog_reports_message_types() as $type) {
    $types[$type] = $type;
  }
  if (!empty($types)) {
    $filters['type'] = array(
      'title' => t('Type'),
      'options' => $types,
    );
  }
  $filters['severity'] = array(
    'title' => t('Severity'),
    'options' => watchdog_severity_levels(),
  );

  return $filters;
}

/**
 * Return form for dblog administration filters.
 *
 * @ingroup forms
 * @see dblog_filter_form_submit()
 * @see dblog_filter_form_validate()
 */
function rawlog_reports_filter_form() {
  $session = &$_SESSION['rawlog_reports_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = rawlog_reports_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#theme' => 'rawlog_reports_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filter['options'],
    );
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from dblog administration filter form.
 */
function rawlog_reports_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['type']) && empty($form_state['values']['severity'])) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from dblog administration filter form.
 */
function rawlog_reports_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = rawlog_reports_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['rawlog_reports_overview_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['rawlog_reports_overview_filter'] = array();
      break;
  }
  return 'admin/reports/rawlog';
}
