<?php


define('RAWLOG_REPORTS_REGEX', '/' .
  'Timestamp: ([A-Za-z, 0-9\/\-\:]*), ' .
  'Site: (http:\/\/[A-Za-z0-9\.]*), ' .
  'Severity: ([\(0-9\) A-Za-z]*), ' .
  'Type: ([A-Za-z0-9 \_\-]*)\, ' .
  'IP Address: ([0-9\.]*), ' .
  'Request URI: (http:\/\/[A-Za-z0-9\.\/]*), ' .
  'Referrer URI: (http:\/\/[A-Za-z0-9\.\/]*), ' .
  'User: ([\(0-9\) A-Za-z]*), ' .
  'Link: ([ ]*), ' .
  'Message: (.*)/'
);

/**
 * Implementation of hook_init().
 */
function rawlog_reports_init() {
  if (arg(0) == 'admin' && arg(1) == 'reports') {
    // Add the CSS for this module
    drupal_add_css(drupal_get_path('module', 'dblog') .'/dblog.css', 'module', 'all', FALSE);
  }
}

/**
 * Implementation of hook_help().
 */
function rawlog_reports_help($path, $arg) {
  switch ($path) {
    case 'admin/help#rawlog':
      $output = '<p>'. t('The rawlog module monitors your system, capturing system events in a log to be reviewed by an authorized individual at a later time. This is useful for site administrators who want a quick overview of activities on their site. The logs also record the sequence of events, so it can be useful for debugging site errors.') .'</p>';
      $output .= '<p>'. t('The rawlog log is simply a list of recorded events containing usage data, performance data, errors, warnings and operational information. Administrators should check the rawlog report on a regular basis to ensure their site is working properly.') .'</p>';
      $output .= '<p>'. t('For more information, see the module page for <a href="@dblog">Rawlog module</a>.', array('@dblog' => 'http://drupal.org/project/rawlog')) .'</p>';
      return $output;
    case 'admin/reports/rawlog':
      return '<p>'. t('The rawlog module monitors your website, capturing system events in a log to be reviewed by an authorized individual at a later time. The rawlog log is simply a list of recorded events containing usage data, performance data, errors, warnings and operational information. It is vital to check the rawlog report on a regular basis as it is often the only way to tell what is going on.') .'</p>';
  }
}

/**
 * Implementation of hook_theme()
 */
function rawlog_reports_theme() {
  return array(
    'rawlog_reports_filters' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function rawlog_reports_menu() {

  $items['admin/reports/rawlog'] = array(
    'title' => 'Recent rawlog entries',
    'description' => 'View events that have recently been logged.',
    'page callback' => 'rawlog_reports_overview',
    'access arguments' => array('access site reports'),
    'weight' => -1,
    'file' => 'includes/admin.inc',
  );
  $items['admin/reports/rawlog-page-not-found'] = array(
    'title' => "Top rawlog 'page not found' errors",
    'description' => "View 'page not found' errors (404s).",
    'page callback' => 'rawlog_reports_top',
    'page arguments' => array('page not found'),
    'access arguments' => array('access site reports'),
    'file' => 'includes/admin.inc',
  );
  $items['admin/reports/rawlog-access-denied'] = array(
    'title' => "Top rawlog 'access denied' errors",
    'description' => "View 'access denied' errors (403s).",
    'page callback' => 'rawlog_reports_top',
    'page arguments' => array('access denied'),
    'access arguments' => array('access site reports'),
    'file' => 'includes/admin.inc',
  );
  $items['admin/reports/rawlog-event/%'] = array(
    'title' => 'Details',
    'page callback' => 'rawlog_reports_event',
    'page arguments' => array(3),
    'access arguments' => array('access site reports'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_cron().
 *
 * Remove expired log messages and flood control events.
 */
function rawlog_reports_cron() {
  // TODO might not have anything to do as this is handled by rawlog
}

/**
 * Theme rawlog_reports filter selector.
 *
 * @ingroup themeable
 */
function theme_rawlog_reports_filters($form) {
  $output = '';
  foreach (element_children($form['status']) as $key) {
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '<div id="rawlog-reports-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
  return $output;
}